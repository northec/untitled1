package com.ljb;
import javax.lang.model.element.Modifier;
import java.lang.reflect.*;
/**
 */
public class helloWorld {
    public static void main(String[] args) {
        // 1. 通过类型class静态变量
        Class clz1 = String.class;
        String str = "Hello";
        // 2. 通过对象的getClass()方法
        Class clz2 = str.getClass();

        System.out.println("clz1:"+clz1);
        System.out.println("clz1 方法:"+clz1.getName());

        Method[] methods = clz1.getMethods();
        for(Method method : methods){
            System.out.print(method.getModifiers());
            System.out.print(" "+method.getReturnType().getName()+" ");
            System.out.println(method.getName()+"();");
        }

    }

}
